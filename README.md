# Arch Linux Keycloak theme

A custom Keycloak theme that resembles the Arch Linux website in terms of coloring scheme and aesthetics.

The custom theme name is `archlinux` and is based on the default theme `keycloak` which in turn is based on `base`. Based means that the custom theme is an extension of the `keycloak` theme that already comes bundled with Keycloak.

## What has been modified

There are five sections that can be customized: login, account, admin console welcome screen and email.  The email content did not need any further changes from the defaults therefore it has not been modified, everything else was customized.

The theme modifications were pretty simple and mostly involved replacing images from Keycloak logo to Arch Linux logo along with some background image changes and some element hiding and sizing fixes.

A Keycloak theme consists of various files but for the custom theme only images, CSS stylesheets and HTML templates (FreeMarker templates) were needed to be modified to achieve the desired results.

- Login screen

  - the background image has been changed
  - the Keycloak logo has been replaced with the Arch Linux logo

- Registration screen

  - added hCaptcha form under reCAPTCHA form according to [the docs of the hCaptcha plugin](https://github.com/p08dev/keycloak-hcaptcha)

- Welcome screen

  - the background image has been changed
  - the Keycloak logo has been replaced with the Arch Linux logo
  - the title text color was changed from black to white due to the dark background
  - the text of the panels and their associated links has been changed to refer to the Arch Linux project instead of the Keycloak one
  - the default footer has been hidden

- Account screen

  - the Keycloak logo on the top left has been replaced with the Arch Linux logo
  - the Keycloak logo during the account console loading animation has been replaced with the Arch Linux logo

- Admin console Screen

  - the Keycloak logo on the top left has been replaced with the Arch Linux logo

## Useful resources

- [Keycloak theming documentation](https://www.keycloak.org/docs/latest/server_development/#_themes)

- [Keycloak default theme resource files](<https://github.com/keycloak/keycloak/tree/main/themes/src/main/resources/theme/keycloak>)

- [Keycloak base theme resource files](https://github.com/keycloak/keycloak/tree/main/themes/src/main/resources/theme/base)

- [Keycloak Docker containers main repository](https://github.com/keycloak/keycloak-containers)

## Additional notes

- When extending a theme you can override individual resources (templates, stylesheets, etc.). To minimize the changes the stylesheets only override what was necessary. Since the custom theme is based on the default `keycloak` theme the new stylesheets import the default stylesheets and through the use of [cascading]([Cascade and inheritance - Learn web development | MDN](https://developer.mozilla.org/en-US/docs/Learn/CSS/Building_blocks/Cascade_and_inheritance)) change only what is necessary.

- If you decide to override HTML templates bear in mind that you may need to update your custom template when upgrading to a new release. Since some HTML templates were modified, a maintenance update to the theme could be necessary.

- While creating a theme it’s a good idea to disable caching as this makes it possible to edit theme resources directly from the `themes` directory without restarting Keycloak. To do this edit `standalone.xml`. For `theme` set `staticMaxAge` to `-1` and both `cacheTemplates` and `cacheThemes` to `false`. **Note**: this is done automatically by the provided `docker-compose` file for local development. See the maintenance guide below for more details.

- Each section that can be modified uses a file called `theme.properties` which allows setting some configuration for the theme such as:

  - parent - Parent theme to extend
  - import - Import resources from another theme
  - styles - Space-separated list of styles to include
  - locales - Comma-separated list of supported locales

  The file sometimes can also include custom properties that can be used from HTML  templates. **Note**: this is quite powerful always try to use properties if at all possible to do theme alterations.

### Licensing details

The custom theme is using the following image resources:

- The Arch Linux "Two-color standard version" and "Two-color inverted version" SVG logos from the [Arch Linux Logos and Artwork webpage](https://www.archlinux.org/art/). The following Arch Linux logos are available for press and other use, subject to the restrictions of a [trademark policy](https://wiki.archlinux.org/index.php/DeveloperWiki:TrademarkPolicy "Arch Linux Trademark Policy").

- The [dark plaster texture background image](https://unsplash.com/photos/gM8igOIP5MA) by [Annie Spratt](https://unsplash.com/@anniespratt) hosted on Unsplash. All images hosted on Unsplash are made to be used freely. Please look at the [license](https://unsplash.com/license) for more information.

### Maintenance guide

Requirements:

- Docker
- Docker Compose
- Java

The theme folder includes a helpful Docker Compose file (`docker-compose.yml`) to enable local theme development and maintenance via a Docker container that spins up a local Keycloak instance that can be quickly accessed and be tampered with. This means that there is no need to touch the actual running Keycloak instance to do theme modifications. It is recommend to test out theme customizations locally first before deploying them to the running instance.

The Docker Compose file is doing the following:

- Spins up a local Keycloak server instance that can be accessed from `http://127.0.0.1:8080`
- Creates a default `admin` user with the same name and password
- Sets the default theme and welcome screen to the `archlinux` theme

**Note**:  this requires [Docker BuildKit](https://docs.docker.com/develop/develop-images/build_enhancements/)


To start the local Keycloak instance and try out some changes:

1. build the jar

   ```shell
   ./gradlew build
   ```

2. run the Docker Compose file in detached mode (detaching is optional)

   ```shell
   DOCKER_BUILDKIT=1 docker-compose up -d
   ```

**Note**: Any local changes require rebuild and recreation of the container

```shell
./gradlew clean build
DOCKER_BUILDKIT=1 docker-compose up -d --force-recreate
```

### Git history

This repository was created from the `roles/keycloak/files/theme` directory in the [archlinux/infrastructure@abdf99cd](https://gitlab.archlinux.org/archlinux/infrastructure/-/tree/abdf99cd3cec2f65f8bfdaec921e6140a52e64bc/roles/keycloak/files/theme) repository with the following commands:

```sh
git clone ssh://git@gitlab.archlinux.org:222/archlinux/infrastructure.git
cd infrastructure
git checkout abdf99cd3cec2f65f8bfdaec921e6140a52e64bc
git filter-branch --subdirectory-filter roles/keycloak/files/theme --msg-filter 'sed "s/^keycloak: //"'
cd ../
mkdir keycloak-archlinux-theme
cd keycloak-archlinux-theme
git init
git remote add infra ../infrastructure
git fetch infra HEAD
git remote remove infra
git git rebase FETCH_HEAD
git remote add origin ssh://git@gitlab.archlinux.org:222/archlinux/keycloak-archlinux-theme.git
git push --set-upstream origin master
```
