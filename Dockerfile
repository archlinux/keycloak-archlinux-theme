FROM quay.io/keycloak/keycloak:latest as builder

ENV KC_DB=postgres
RUN /opt/keycloak/bin/kc.sh build

FROM quay.io/keycloak/keycloak:latest
COPY --from=builder /opt/keycloak/lib/quarkus/ /opt/keycloak/lib/quarkus/
COPY --chmod=755 ./scripts/build-and-start.sh /opt/keycloak/build-and-start.sh
# RUN ["chmod", "+x", "/opt/keycloak/build-and-start.sh"]

WORKDIR /opt/keycloak

ENV KEYCLOAK_ADMIN=admin
ENV KEYCLOAK_ADMIN_PASSWORD=admin
ENV KC_DB_URL_HOST=db
ENV KC_DB_URL_DATABASE=keycloak
ENV KC_DB_USERNAME=postgres
ENV KC_DB_PASSWORD=postgres
ENV KC_HTTP_ENABLED=true
ENTRYPOINT ["/opt/keycloak/build-and-start.sh"]
