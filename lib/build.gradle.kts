plugins {
    `java-library`
}

repositories {
    mavenCentral()
}

tasks {
    jar {
        archiveBaseName.set(rootProject.name)
    }

    withType<AbstractArchiveTask>().configureEach {
        setProperty("preserveFileTimestamps", false)
        setProperty("reproducibleFileOrder", true)
    }
}